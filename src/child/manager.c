#include <signal.h>
#include "../../lib/mainlib.h"
#include "../../lib/childlib.h"

int masterChannel;
char buffer[BUFFERSIZE];
char wesBuffer[BUFFERSIZE];

int mySocket, wesChannel;

char pathFile[64];
FILE *G18txt;

int pfcPID[3], pfcCHANNEL[3];

// ----------------------------------------------------------------------------------------- Headers
int createWriters();

int acceptSignals();

char *nextString();

// ----------------------------------------------------------------------------------------- Main

int main(int argc, char **argv) {
    strcpy(pathFile, argv[0]);
    childMain(&masterChannel, buffer, "manager");
}

int setup() {
    createSocket(SOCKET_MANAGER, 3, &mySocket);
    G18txt = fopen(pathFile, "r");
    if (mySocket <= 0 || G18txt == NULL) return 1;
    return createWriters();
}

int preparation() {
    return acceptSignals();
}

int connection() {
    sprintf(buffer, "%d %d %d", pfcPID[0], pfcPID[1], pfcPID[2]);
    int error = write(masterChannel, buffer, BUFFERSIZE) <= 0;
    int signals = acceptSignals();
    connectSocket(SOCKET_WES, &wesChannel);
    return error || signals || wesBuffer <= 0;
}

int routine() {
    char *str = nextString();
    if (str != NULL) {
        write(pfcCHANNEL[0], str, BUFFERSIZE);
        write(pfcCHANNEL[1], str, BUFFERSIZE);
        write(pfcCHANNEL[2], str, BUFFERSIZE);
        write(wesChannel, str, BUFFERSIZE);
    }
    return str == NULL;
}

void closeProcess() {
    for (int i = 0; i < 3; ++i) {
        kill(pfcPID[i], SIGTERM);
        close(pfcCHANNEL[i]);
    }
    close(masterChannel);
    exit(0);
}

// ----------------------------------------------------------------------------------------- Complementary

int createWriters() {
    char *pfcName[3] = {"pfc1", "pfc2", "pfc3"};
    for (int i = 0; i < 3; ++i) createProcess(pfcName[i], NULL, &pfcPID[i]);
    for (int i = 0; i < 3; ++i) {
        int tempChnl = accept(mySocket, NULL, NULL);
        read(tempChnl, buffer, BUFFERSIZE);
        int pid = parseInt(buffer);
        for (int j = 0; j < 3; ++j) {
            if (pid != pfcPID[j]) continue;
            sendSignal(tempChnl, SIG_CONT); // start PFC setup()
            pfcCHANNEL[j] = tempChnl;
            break;
        }
    }
    return acceptSignals(); // accept PFC setup() return value
}

int acceptSignals() {
    int count = 3;
    for (int i = 0; i < 3; ++i) {
        read(pfcCHANNEL[i], buffer, BUFFERSIZE);
        if (gotSUCCSignal(buffer)) {
            count--;
            sendSignal(pfcCHANNEL[i], SIG_CONT);
        }
    }
    return count;
}

char *nextString() {
    char *res;
    char temp[BUFFERSIZE];
    while (1) {
        res = fgets(temp, BUFFERSIZE, G18txt);
        if (res == NULL) break;
        else if (strncmp(res, "$GPGLL", 6) != 0) continue;
        temp[strcspn(temp, "\r\n")] = '\0';
        break;
    }
    return res;
}
