#include "../../lib/mainlib.h"
#include "../../lib/childlib.h"

int masterChannel;
char buffer[BUFFERSIZE];

int mySocket;

int chPIDS[3], chCHANNEL[3];

// ----------------------------------------------------------------------------------------- Headers

int createReaders();

int acceptSignals();

// ----------------------------------------------------------------------------------------- Main

int main() {
    childMain(&masterChannel, buffer, "transducer");
}

int setup() {
    createSocket(SOCKET_TRANSDUCER, 3, &mySocket);
    return mySocket <= 0 || createReaders();
}

int preparation() {
    return acceptSignals();
}

int connection() {
    return acceptSignals();
}

int routine() {
    for (int i = 0; i < 3; ++i) {
        write(chCHANNEL[i], SIG_CONT, BUFFERSIZE);
    }
    return 0;
}

void closeProcess() {
    for (int i = 0; i < 3; ++i) {
        sendSignal(chCHANNEL[i], SIG_TERM);
        wait(NULL);
        close(chCHANNEL[i]);
    }
    close(masterChannel);
    exit(0);
}

// ----------------------------------------------------------------------------------------- OTHER

int createReaders() {
    char *names[3] = {"channel1", "channel2", "channel3"};
    for (int i = 0; i < 3; ++i)
        createProcess(names[i], NULL, &chPIDS[i]);
    for (int i = 0; i < 3; ++i) {
        int chConn = accept(mySocket, NULL, NULL);
        read(chConn, buffer, BUFFERSIZE);
        int pid = parseInt(buffer);
        for (int j = 0; j < 3; ++j) {
            if (pid != chPIDS[j]) continue;
            chCHANNEL[j] = chConn;
            sendSignal(chConn, SIG_CONT); // start CHANNEL setup()
            break;
        }
    }
    return acceptSignals(); // accept CHANNEL setup() return value
}

int acceptSignals() {
    int count = 3;
    for (int i = 0; i < 3; ++i) {
        read(chCHANNEL[i], buffer, BUFFERSIZE);
        if (gotSUCCSignal(buffer)) {
            count--;
            sendSignal(chCHANNEL[i], SIG_CONT);
        }
    }
    return count;
}
