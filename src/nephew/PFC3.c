#include "../../lib/mainlib.h"
#include "../../lib/nephewlib.h"

int managerChannel;
char buffer[BUFFERSIZE];

FILE *channel; // file
int multiplier = 1;

int it = 0;
double lastLon, lastLat;

// ----------------------------------------------------------------------------------------- Headers

void signal_handler(int signum) {
    if (signum == SIGUSR1) multiplier = 4;
}

// ----------------------------------------------------------------------------------------- Main

int main() {
    signal(SIGUSR1, signal_handler);
    pfcMain(&managerChannel, buffer);
}

int setup() {
    // do nothing
    return 0;
}

int preparation() {
    int timeout = WAITTIME;
    while (timeout-- > 0 && channel == NULL) {
        channel = fopen(CHANNEL_FILE, "a");
        sleep(1);
    }
    int result = channel == NULL;
    return result;
}

int connection() {
    // do nothing
    return 0;
}

void routine() {
    double lon, lat, distance;
    extractValues(buffer, &lon, &lat);
    distance = (it++ > 0) ? distanceBetween(lastLon, lastLat, lon, lat) * multiplier : 0;
    multiplier = 1;
    sprintf(buffer, "%f", distance);
    fflush(stdout);
    fputs(buffer, channel);
    fflush(channel);
    lastLon = lon;
    lastLat = lat;
}

void closeProcess() {
    fclose(channel);
    close(managerChannel);
    exit(0);
}