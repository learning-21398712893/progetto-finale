#include "lib/mainlib.h"

void removeLogFile();

void checkProcess(char *phasename);

void sighandler(int signum);

//

char *G18FilePath;
char *names[5] = {"manager", "transducer", "wes", "switch", "failgen"};
int pids[5], chnl[5];

char buffer[BUFFERSIZE];
int mySocket;

int go = 1; // routine continue

int main(int argc, char **argv) {
    signal(SIGINT, sighandler);
    printf(">>> Programma e' in fase di avvio...\n");
    mkdir("../logs", 0777);
    removeLogFile();
    G18FilePath = argv[1];
    master_init();
    master_setup();
    master_preparation();
    master_connection();
    master_routine();
    master_exit();
}

void master_init() {
    int created = 0;
    createSocket(SOCKET_MASTER, 5, &mySocket);
    for (int i = 0; i < 5; ++i) {
        if (i == 0) createProcess(names[i], G18FilePath, &pids[i]);
        else createProcess(names[i], NULL, &pids[i]);
    }
    for (int i = 0; i < 5; ++i) {
        int tempChnl = accept(mySocket, NULL, NULL);
        read(tempChnl, buffer, BUFFERSIZE);
        int pid = parseInt(buffer);
        for (int j = 0; j < 5; ++j) {
            if (pid != pids[j]) continue;
            sendSignal(tempChnl, SIG_CONT);
            chnl[j] = tempChnl;
            created++;
            break;
        }
    }
}

void master_setup() { checkProcess("setup"); }

void master_preparation() { checkProcess("preparazione"); }

void master_connection() {
    read(chnl[0], buffer, BUFFERSIZE);
    write(chnl[3], buffer, BUFFERSIZE);
    write(chnl[4], buffer, BUFFERSIZE);
    checkProcess("connection");
}

void master_routine() {
    printf(">>> Programma avviato correttamente\n\n");
    int queue[5] = {4, 0, 1, 2, 3};
    while (go) {
        for (int j = 0; j < 5; ++j) {
            char *pname = names[queue[j]];
            sendSignal(chnl[queue[j]], SIG_CONT);
            read(chnl[queue[j]], buffer, BUFFERSIZE);
            if (!gotSUCCSignal(buffer)) go = 0;
            usleep(200000);
            if (!go) break;
        }
        fflush(stdout);
        memset(buffer, '\0', BUFFERSIZE);
    }
}

void master_exit() {
    for (int i = 0; i < 5; ++i) {
        char *pname = names[i];
        sendSignal(chnl[i], SIG_TERM);
        wait(NULL);
        close(chnl[i]);
    }
    remove(SOCKET_WES);
    remove(SOCKET_TRANSDUCER);
    remove(SOCKET_MANAGER);
    remove(SOCKET_MASTER);
    close(mySocket);
    printf(">>> Programma terminato\n");
    exit(0);
}

//

void sighandler(int signum) {
    if (signum == SIGINT || signum == SIGTERM) go = 0;
}

void checkProcess(char *phasename) {
    int count = 0;
    for (int i = 0; i < 5; ++i) {
        read(chnl[i], buffer, BUFFERSIZE);
        if (gotSUCCSignal(buffer)) count++;
        else printf(">>> %s non e' riuscito a completare la %s\n", names[i], phasename);
    }
    if (count != 5) master_exit();
    else for (int i = 0; i < 5; ++i) sendSignal(chnl[i], SIG_CONT);
}

void removeLogFile() {
    remove("../logs/failures.log");
    remove("../logs/speedPFC1.log");
    remove("../logs/speedPFC2.log");
    remove("../logs/speedPFC3.log");
    remove("../logs/status.log");
    remove("../logs/switch.log");
}

